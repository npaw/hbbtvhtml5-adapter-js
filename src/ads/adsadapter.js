var youbora = require('youboralib')
var manifest = require('../../manifest.json')
var hbbtvHtml5Adapter = require('../adapter')

youbora.adapters.HbbtvHtml5Ads = hbbtvHtml5Adapter.extend({
    /** Override to return current plugin version */
    getVersion: function () {
        return manifest.version + '-' + manifest.name + 'Ads-' + manifest.tech
    },

    quartileCheckerStart: function () {
        this._quartileInterval = setInterval(this.checkQuartile.bind(this), 1000)
    },

    quartileCheckerEnd: function () {
        if (this._quartileInterval) {
            clearInterval(this._quartileInterval)
            this._quartileNumber = 0
        }
    },

    checkQuartile: function () {
        try {
            if (this.plugin) {
                var adDuration = this.plugin.getAdDuration()
                if (adDuration > 0) {
                    var currentQuartile = parseInt((this.getPlayhead() / adDuration) * 4)
                    if (!isNaN(currentQuartile) && currentQuartile !== Infinity) {
                        if (currentQuartile > 4) {
                            currentQuartile = 4
                        }
                        if (currentQuartile !== this._quartileNumber) {
                            this._quartileNumber = currentQuartile
                            this.fireQuartile(currentQuartile)
                        }
                        if (currentQuartile >= 4) {
                            this.quartileCheckerEnd()
                        }
                    }
                }
            }
        } catch (e) {}
    },

    /** Register listeners to this.player. */
    registerListeners: function () {
        if (this._quartileInterval) {
            clearInterval(this._quartileInterval)
        }
        this._quartileInterval = null
        this._quartileNumber = 0
        this.states = [
            'STOPPED', // 0
            'PLAYING', // 1
            'PAUSED', // 2
            'CONNECTING', // 3
            'BUFFERING', // 4
            'FINISHED', // 5
            'ERROR' // 6
        ]
        // Enable playhead monitor (buffer = true, seek = false)
        this.monitorPlayhead(true, true)

        if (this.player) {
            this.player.onPlayStateChange = function (e) {
                this.playStateChangeListener()
            }.bind(this)
        } else {
            youbora.Log.error('The player reference set to the adapter is invalid.')
        }
    },

    playStateChangeListener: function () {
        youbora.Log.debug('Play State: ' + this.states[this.player.playState] + ':' + this.player.playState)
        switch (this.player.playState) {
            case 0: // Stopped
                this.quartileCheckerEnd()
                this.fireStop()
                break
            case 1: // Playing
                this.fireResume()
                this.fireSeekEnd()
                this.fireJoin()
                this.quartileCheckerStart()
                break
            case 2: // Paused
                this.firePause()
                break
            case 3: // Connecting
            case 4: // Buffering
                this.fireStart()
                this.quartileCheckerStart()
                break
            case 5: // Finished
                this.quartileCheckerEnd()
                this.fireStop()
                break
            case 6: // Error
                this.fireError()
                break
        }
    }
})

module.exports = youbora.adapters.HbbtvHtml5Ads
