var youbora = require('youboralib')
var manifest = require('../manifest.json')

youbora.adapters.HbbtvHtml5 = youbora.Adapter.extend({
  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    return this.player.playPosition / 1000
  },

  /** Override to return video duration */
  getDuration: function () {
    return this.player.playTime / 1000
  },

  /** Override to return resource URL. */
  getResource: function () {
    return this.player.data || this.player.src
  },

  /** Override to return player's name */
  getPlayerName: function () {
    return 'Hbbtv'
  },

  /** Register listeners to this.player. */
  registerListeners: function () {
    this.improveBuffer = false
    this.states = [
      'STOPPED', // 0
      'PLAYING', // 1
      'PAUSED', // 2
      'CONNECTING', // 3
      'BUFFERING', // 4
      'FINISHED', // 5
      'ERROR' // 6
    ]
    // Enable playhead monitor (buffer = true, seek = false)
    this.monitorPlayhead(true, true)

    if (this.player) {
      this.player.onPlayStateChange = function (e) {
        this.playStateChangeListener()
      }.bind(this)
    } else {
      youbora.Log.error('The player reference set to the adapter is invalid.')
    }
  },

  unregisterListeners: function () {
    this.player.onPlayStateChange = null;
    this.monitorPlayhead(false, false);
  },

  playStateChangeListener: function () {
    youbora.Log.debug('Play State: ' + this.states[this.player.playState] + ':' + this.player.playState)
    switch (this.player.playState) {
      case 0: // Stopped
        this.fireStop({}, 'stopped');
        break;
      case 1: // Playing
        this.fireResume({}, 'playing')
        this.fireSeekEnd({}, 'playing')
        this.fireJoin({}, 'playing')
        this.fireBufferEnd({}, 'playing')
        break
      case 2: // Paused
        this.firePause({}, 'paused')
        break
      case 3: // Connecting
        this.fireStart({}, 'connecting')
        break
      case 4: // Buffering
        if (this.improveBuffer && this.flags.isJoined) {
          this.fireBufferBegin({}, false, 'buffering')
        } else {
          this.fireStart({}, 'buffering')
        }
        break
      case 5: // Finished
        this.fireStop({}, 'finished')
        break
      case 6: // Error
        this.fireError(undefined, undefined, undefined, undefined, 'error')
        break
    }
  }
})

module.exports = youbora.adapters.HbbtvHtml5
