## [6.8.14] - 2024-12-09
### Added
- Add `improveBuffer` option to improve buffer recognition when `playheadMonitor` does not work properly.
- Packaged with `lib 6.8.59`

## [6.8.13] - 2023-06-21
### Added
- Adding triggeredEvents on all video adapter methods
- Packaged with `lib 6.8.47`

## [6.8.12] - 2023-05-31
### Added
- `unregisterListeners()` function.

## [6.8.11] - 2023-03-21
### Fixed
- Get ad duration from plugin instead of adapter directly, to prioritize adDuration set on options object

## [6.8.10] - 2023-03-07
### Added
- Create an AdsAdapter based on httbvhtml5 adapter, checking playhead to report adQuartiles

## [6.8.9] - 2022-10-18
### Library
- Packaged with `lib 6.8.31`

## [6.8.8] - 2022-07-20
### Library
- Packaged with `lib 6.8.27`

## [6.8.7] - 2022-06-07
### Library
- Packaged with `lib 6.8.22`

## [6.8.6] - 2022-04-04
### Library
- Packaged with `lib 6.8.17`

## [6.8.5] - 2022-03-31
### Library
- Packaged with `lib 6.8.16`

## [6.8.4] - 2021-11-23
### Library
- Packaged with `lib 6.8.9`

## [6.8.3] - 2021-11-22
### Library
- Packaged with `lib 6.8.8`

## [6.8.2] - 2021-11-11
### Library
- Packaged with `lib 6.8.7`

## [6.8.1] - 2021-11-08
### Library
- Packaged with `lib 6.8.6`

## [6.8.0] - 2021-10-26
### Library
- Packaged with `lib 6.8.5`

## [6.7.3] - 2021-07-23
### Fixed
- Library version dependency 

## [6.7.2] - 2021-07-21
### Fixed
- Now the resource is get from player.data instead of player.src
### Added
- Code refactor

## [6.7.1] - 2021-06-18
### Library
- Packaged with `lib 6.7.40`

## [6.7.0] - 2020-10-13
### Library
- Packaged with `lib 6.7.17`

## [6.2.0] - 2018-05-09
### Library
- Packaged with `lib 6.2.4`

## [6.1.0] - 2018-03-15
### Library
- Packaged with `lib 6.1.13`
